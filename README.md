# Angular - Product Registration CRUD 

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.2.3.

## Frontend server

Run `npm start` in `/frontend` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Backend server

Run `npm start` in `/backend` folder for a dev server. Navigate to `http://localhost:3001/`. The app will automatically reload if you change any of the source files.

